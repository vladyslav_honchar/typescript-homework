export interface IMovie {
    id: number;
    poster_path: string | null;
    release_date?: string;
    overview?: string;
    backdrop_path?: string;
    title?: string;
}

export interface IMovieList {
    page: number,
    total_pages: number,
    results: IMovie[]
}

export interface IQuery {
    query?: string,
    page?: number
}
