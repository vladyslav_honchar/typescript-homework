import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Button } from "../enums/buttons";
import { Container } from "../enums/containers";
import { IMovieList } from "../models/Interfaces";
import { clearMainContent } from "./clearMainContent";
import { createElementMovie } from "./createElementMovie";
import { createLoadMoreButton } from "./LoadMoreButton";
import { movieListMapper } from "./mapper";
import { setRandomMovie } from "./randomMovie";
import { getData } from "./requestHelper";

export const initEvent = async (button: Button, apiMethod: ApiMethod, apiUrl: ApiUrl, page?: number) => {
    const pressedButton = document.getElementById(button);
    pressedButton?.addEventListener('click', async () => {
        let movies: IMovieList;
        let query = '';
        if (button === Button.SEARCH) {
            query = (<HTMLInputElement>document.getElementById('search')).value;
            movies = movieListMapper(await getData(apiMethod, apiUrl, { query: query, page: page }));
        } else {
            movies = movieListMapper(await getData(apiMethod, apiUrl));
        }
        setRandomMovie(movies.results);
        clearMainContent();
        movies.results.forEach((movie) => {
            createElementMovie(Container.FILM, movie);
        });
        createLoadMoreButton(movies.page, movies.total_pages, query, apiUrl);
    })
}