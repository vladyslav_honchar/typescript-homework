import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Button } from "../enums/buttons";
import { initEvent } from "../helpers/initEvent";

export const initSearch = async () => {
    await initEvent(Button.SEARCH, ApiMethod.GET, ApiUrl.SEARCH);
};