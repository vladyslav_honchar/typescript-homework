export enum Container {
    FILM = 'film-container',
    FAVOURITE = 'favorite-movies'
}