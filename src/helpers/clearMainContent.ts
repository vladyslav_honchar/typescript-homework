export const clearMainContent = (): void => {
    const myNode = document.getElementById("film-container");
    while (myNode?.firstChild) {
        myNode.removeChild(<Node>myNode.lastChild);
    }
}