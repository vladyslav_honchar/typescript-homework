import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Container } from "../enums/containers";
import { IMovie } from "../models/Interfaces";
import { getData } from "./requestHelper";

export const createElementMovie = async (container: Container, movie: IMovie) => {
    const favouriteNode = document.getElementById(Container.FAVOURITE);
    const element = document.createElement('div');
    const elementChild = document.createElement('div');
    elementChild.classList.add('card', 'shadow-sm');

    const imgElem = document.createElement('img');
    if (movie.poster_path === null) {
        imgElem.src = 'https://worldsrc.uproxy.co/assets/images/no-poster.png'
    } else {
        imgElem.src = `https://image.tmdb.org/t/p/original${movie.poster_path}`;
    }

    const svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElem.setAttribute('stroke', 'red');

    if (container === Container.FILM) {
        element.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
        svgElem.setAttribute('id', movie.id.toString());
        if (localStorage.getItem(svgElem.id)) {
            svgElem.setAttribute('fill', 'red'); // "" filled
        } else {
            svgElem.setAttribute('fill', '#ff000078'); // "" unfilled
        }
        svgElem.addEventListener('click', async () => {
            if (localStorage.getItem(svgElem.id)) {
                svgElem.setAttribute('fill', '#ff000078');
                localStorage.removeItem(svgElem.id);
                const favourSvg = document.getElementById('f' + svgElem.id);
                const elemToDelete = favourSvg?.parentElement?.parentElement;
                favouriteNode?.removeChild(<HTMLElement>elemToDelete);

            } else {
                svgElem.setAttribute('fill', 'red');
                localStorage.setItem(svgElem.id, svgElem.id);

                const movie = await getData(ApiMethod.GET, ApiUrl.MOVIE, undefined, svgElem.id);
                createElementMovie(Container.FAVOURITE, movie);
            }

            console.log(Object.entries(localStorage));

        });

    } else if (container === Container.FAVOURITE) {
        element.classList.add('col-12', 'p-2');
        svgElem.setAttribute('id', 'f' + movie.id);
        svgElem.setAttribute('fill', 'red');
        svgElem.addEventListener('click', async () => {
            const id = svgElem.id.substring(1);
            const elemOnPage = document.getElementById(id);
            if (elemOnPage) {
                elemOnPage.setAttribute('fill', '#ff000078');
            }
            localStorage.removeItem(id);
            favouriteNode?.removeChild(<HTMLElement>svgElem.parentElement?.parentElement);
        });
    }


    svgElem.setAttribute('width', '50');
    svgElem.setAttribute('height', '50');
    svgElem.classList.add('bi', 'bi-heart-fill', 'position-absolute', 'p-2');
    svgElem.setAttribute('viewBox', '0 -2 18 22');


    const pathElem = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathElem.setAttribute('fill-rule', 'evenodd');
    pathElem.setAttribute('d', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');

    const divText = document.createElement('div');
    divText.classList.add('card-body');
    const paraText = document.createElement('p');

    paraText.classList.add('card-text', 'truncate');
    paraText.innerHTML = movie.overview || 'No overview';

    const divFlex = document.createElement('div');
    divFlex.classList.add('d-flex', 'justify-content-between', 'align-items-center');

    const smallElem = document.createElement('small');
    smallElem.classList.add('text-muted');
    smallElem.innerHTML = movie.release_date || "Unknown release date";


    divFlex.appendChild(smallElem);
    divText.appendChild(paraText);
    divText.appendChild(divFlex);
    svgElem.appendChild(pathElem);
    elementChild.append(imgElem);
    elementChild.append(svgElem);
    elementChild.append(divText);
    element.appendChild(elementChild);
    console.log(element);

    const containerNode = document.getElementById(container);
    containerNode?.appendChild(element);
}