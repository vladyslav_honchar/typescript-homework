import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Button } from "../enums/buttons"
import { Container } from "../enums/containers";
import { IMovieList } from "../models/Interfaces";
import { clearMainContent } from "./clearMainContent";
import { createElementMovie } from "./createElementMovie";
import { movieListMapper } from "./mapper";
import { getData } from "./requestHelper";

export const createLoadMoreButton = (page: number, total_pages: number, query: string, apiUrl: ApiUrl) => {
    const previousButton = document.getElementById(Button.LOAD_MORE);
    const newButton = <HTMLButtonElement>(previousButton?.cloneNode(true));
    previousButton?.parentElement?.appendChild(newButton);
    previousButton?.parentElement?.removeChild(previousButton);

    if ((total_pages === 0) || (total_pages === page)) {
        newButton.style.display = 'none';
        return;
    } else {
        newButton.style.display = 'block    ';
    }


    newButton.addEventListener('click', async () => {

        page += 1;
        let movies: IMovieList;
        if (apiUrl === ApiUrl.SEARCH) {
            movies = movieListMapper(await getData(ApiMethod.GET, apiUrl, { query: query, page: page }));
        } else {
            movies = movieListMapper(await getData(ApiMethod.GET, apiUrl, { page: page }));
        }
        movies.results.forEach((movie) => {
            createElementMovie(Container.FILM, movie);
        });
        if (page === total_pages) newButton.style.display = 'none';



    })


}