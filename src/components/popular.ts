import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Button } from "../enums/buttons";
import { initEvent } from "../helpers/initEvent";

export const initPopular = async () => {
    await initEvent(Button.POPULAR, ApiMethod.GET, ApiUrl.POPULAR);
}