import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Button } from "../enums/buttons";
import { initEvent } from "../helpers/initEvent";

export const initTopRated = async () => {
    await initEvent(Button.TOP_RATED, ApiMethod.GET, ApiUrl.TOP_RATED);
};