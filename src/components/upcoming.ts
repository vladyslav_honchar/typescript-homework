import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Button } from "../enums/buttons";
import { initEvent } from "../helpers/initEvent";

export const initUpcoming = async () => {
    await initEvent(Button.UPCOMING, ApiMethod.GET, ApiUrl.UPCOMING);
};