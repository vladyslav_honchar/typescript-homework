import { ApiMethod, ApiUrl } from "../enums/apiMethod";
import { Container } from "../enums/containers";
import { createElementMovie } from "../helpers/createElementMovie";
import { movieMapper } from "../helpers/mapper";
import { getData } from "../helpers/requestHelper";
import { IMovie } from "../models/Interfaces";

export const initFavorite = async () => {
    const favouriteNode = <HTMLElement>document.getElementById(Container.FAVOURITE);
    while (favouriteNode?.firstChild) {
        favouriteNode.removeChild(<Node>favouriteNode.lastChild);
    }

    Object.keys(localStorage).forEach(async (key) => {
        if (key !== 'undefined') {
            const movie: IMovie = movieMapper(await getData(ApiMethod.GET, ApiUrl.MOVIE, undefined, key));
            createElementMovie(Container.FAVOURITE, movie);
        }
    });
};