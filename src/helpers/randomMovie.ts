import { IMovie } from "../models/Interfaces";

export const setRandomMovie = (movies: IMovie[]) => {
    movies = movies.filter((movie) => movie.backdrop_path && movie.overview);
    if (movies.length !== 0) {
        const randomMovieDiv = <HTMLDivElement>document.getElementById('random-movie');
        const randomInt = getRandomInt(movies.length);

        randomMovieDiv.style.backgroundImage = `url(https://image.tmdb.org/t/p/original${movies[randomInt].backdrop_path})`;
        randomMovieDiv.style.backgroundSize = 'contain';

        const randomMovieH1 = <HTMLHeadingElement>document.getElementById('random-movie-name');
        randomMovieH1.innerHTML = <string>movies[randomInt].title;

        const randomMovieDesctiption = <HTMLHeadingElement>document.getElementById('random-movie-description');
        randomMovieDesctiption.innerHTML = <string>movies[randomInt].overview;

    }

}

const getRandomInt = (max: number) => {
    return Math.floor(Math.random() * max);
}