import { stringifyUrl } from "query-string";
import { ApiMethod } from "../enums/apiMethod";



export const api = async (method: ApiMethod, query: { api_key: string, page?: number, query?: string }, apiUrl: string) => {
    const response = await fetch(stringifyUrl({ url: apiUrl, query }, { skipNull: true }), { method });
    const data = await response.json();
    return data;
}