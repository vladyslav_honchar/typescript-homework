import { IMovie, IMovieList } from "../models/Interfaces";

export const movieMapper = ({ id, poster_path, release_date, overview, backdrop_path, title }: {
    id: number, poster_path: string | null,
    release_date?: string,
    overview?: string,
    backdrop_path?: string,
    title?: string
}): IMovie => ({
    id,
    poster_path,
    release_date,
    overview,
    backdrop_path,
    title
});

export const movieListMapper = (data: any): IMovieList => {
    let { total_pages, page, results }: { total_pages: number, page: number, results: IMovie[] } = data;
    results = results.map(movieMapper)

    const movieList: IMovieList = {
        total_pages,
        page,
        results
    };
    return movieList;
}

