import { ApiMethod, ApiUrl } from '../enums/apiMethod';
import { api } from '../helpers/apiHelper';
import { IQuery } from '../models/Interfaces';

export const getData = async (method: ApiMethod, url: ApiUrl, queryString?: IQuery, id = '') => {
    const data = await api(method, {
        query: queryString?.query,
        page: queryString?.page,
        api_key: '337cd79c0767c9f7ab44987c4c2d2a5a',
    }, url + id);
    console.log("data", data);

    return data;

}
