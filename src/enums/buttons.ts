export enum Button {
    SEARCH = 'submit',
    POPULAR = 'popular',
    TOP_RATED = 'top_rated',
    UPCOMING = 'upcoming',
    LOAD_MORE = 'load-more',
}