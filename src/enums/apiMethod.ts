export enum ApiMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
}

export enum ApiUrl {
    MOVIE = 'https://api.themoviedb.org/3/movie/',
    SEARCH = 'https://api.themoviedb.org/3/search/movie',
    POPULAR = 'https://api.themoviedb.org/3/movie/popular',
    TOP_RATED = 'https://api.themoviedb.org/3/movie/top_rated',
    UPCOMING = 'https://api.themoviedb.org/3/movie/upcoming',

}